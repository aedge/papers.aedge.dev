import { addDays, parse, format } from "date-fns";
import { ru } from "date-fns/locale";

function ensureDate(argument) {
  if (argument instanceof Date) return argument;
  return parse(argument, "yyyy-MM-dd", new Date());
}

window.addDays = (date, amount) => addDays(ensureDate(date), amount);
window.formatDate = (date, fmt) => format(ensureDate(date), fmt, { locale: ru });

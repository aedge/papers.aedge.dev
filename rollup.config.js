import { nodeResolve } from "@rollup/plugin-node-resolve";
import { terser } from "rollup-plugin-terser";

export default {
  input: "src/js/plugins.js",
  output: {
    dir: "dist/js",
    format: "iife",
    compact: true,
    banner: "/*! Includes code from date-fns.org (c) Sasha Koss and Lesha Koss, licensed MIT */",
  },
  plugins: [nodeResolve(), terser()],
};
